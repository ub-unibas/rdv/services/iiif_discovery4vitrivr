import json
from flask import request, Response
from flask_restx import Resource, Namespace
import pygsheets
from cache_decorator_redis_ubit import CacheDecorator12h
from gdspreadsheets_ubit import authorize_pyg, PygExtendedClient

ns_iiifdiscovery = Namespace('discovery', description="IIIF Discovery API for vitrivr with Google Spreadsheet Backend")

@ns_iiifdiscovery.route('/activity/all-changes', methods=['GET'], defaults={"pagid": None})
@ns_iiifdiscovery.route('/activity/page-<pagid>', methods=['GET'])
class IIIFDiscovery(Resource):

    @classmethod
    def get(self, pagid):
        gd_service_file = os.getenv("GD_SERVICE_FILE")
        spreadsheet_id = "149QW7Ck8DRNNOSJzRJ1WN9JrUskPVOQ-ojg2SCmecMY"
        sheet_name = "IIIF"
        PygExtendedClient.gc = authorize_pyg(service_file=gd_service_file, cache_decorator=CacheDecorator12h)
        try:
            spreadsheet = PygExtendedClient.gc.open_by_key(spreadsheet_id)
            sheet = spreadsheet.worksheet_by_title(sheet_name)
            iiif_resources = sheet.get_all_records()
            iiif_resources = [x for x in sorted(iiif_resources, key=lambda x: x.get("Datum hinzugefügt",""))
                              if x.get("Datum hinzugefügt","") and x.get("IIIF-Manifest Link")]
            manif_per_page = 100
            #lastCrAWL
            if not pagid:
                result= self.get_all_changes(len(iiif_resources), manif_per_page)
                return self.return_response(result)
            else:
                result = self.get_page(iiif_resources, pagid, manif_per_page)
                return self.return_response(result)
        except pygsheets.exceptions.WorksheetNotFound:
            print("Sheet {} for spreadsheet {} did not exist, created".format(sheet_name, spreadsheet_id))
            return 401

    @staticmethod
    def return_response(r: str) -> Response:
        """ return flask Response, set Access-Control-Allow-Origin

        :param r: IIIF-Object
        :return: flask Response with necessary headers
        """
        if isinstance(r, dict):
            r = json.dumps(r)
        resp = Response(r)
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = 'application/ld+json;profile="http://iiif.io/api/discovery/1/context.json"'
        return resp

    @staticmethod
    def url_prefix():
        return f"{request.root_url}v1/discovery"


    @staticmethod
    def get_page(iiif_resources, pagid, manif_per_page=10):
        pagenum = int(pagid)
        prevnext_page = {}

        if int(pagid) > 0:
            prevnext_page.update({"prev": {
                "id": f"{IIIFDiscovery.url_prefix()}/activity/page-{str(pagenum - 1)}",
                      "type": "OrderedCollectionPage"}})
        if int(pagid) < len(iiif_resources)/manif_per_page - 1:
            prevnext_page.update({"next": {
                "id": f"{IIIFDiscovery.url_prefix()}/activity/page-{str(pagenum + 1)}",
                "type": "OrderedCollectionPage"}})

        manifests = []
        for iiif_res in iiif_resources[pagenum*manif_per_page:pagenum*manif_per_page+manif_per_page]:
            manifests.append({
                "type": "Update",
                "object": {
                    "id": iiif_res.get("IIIF-Manifest Link"),
                    "name": iiif_res.get("Titel"),
                    "type": "Manifest"
                },
                "endTime": f"{iiif_res.get('Datum hinzugefügt')}T00:00:00Z",
            })

        page = {
            "@context": "http://iiif.io/api/discovery/1/context.json",
            "id": f"{IIIFDiscovery.url_prefix()}/activity/page-{pagid}",
            "type": "OrderedCollectionPage",
            "startIndex": int(pagid) * manif_per_page,
            "partOf": {
                "id": f"{IIIFDiscovery.url_prefix()}/activity/all-changes",
                "type": "OrderedCollection"
            },
            "orderedItems": manifests
        }
        page.update(prevnext_page)
        return page

    @staticmethod
    def get_all_changes(number_of_items, manif_per_page=10):
        ordered_collection ={
            "@context": "http://iiif.io/api/discovery/1/context.json",
            "id": f"{IIIFDiscovery.url_prefix()}/activity/all-changes",
            "type": "OrderedCollection",
            "totalItems": int(number_of_items),
            "seeAlso": [
                {
                    "id": "https://docs.google.com/spreadsheets/d/149QW7Ck8DRNNOSJzRJ1WN9JrUskPVOQ-ojg2SCmecMY",
                    "type": "Dataset",
                    "label": { "en": [ "Google Spreadsheet Definition of dataset" ] },
                }
            ],
            "first": {
                "id": f"{IIIFDiscovery.url_prefix()}/activity/page-0",
                "type": "OrderedCollectionPage"
            },
            "last": {
                "id": f"{IIIFDiscovery.url_prefix()}/activity/page-{int(number_of_items / manif_per_page)}",
                "type": "OrderedCollectionPage"
            }
        }
        return ordered_collection
