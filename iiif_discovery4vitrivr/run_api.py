from iiif_discovery4vitrivr.iiif_discovery import ns_iiifdiscovery
if __name__ == "__main__":
    from flask import Flask, Blueprint
    from flask_restx import Api
    from flask_cors import CORS
    from flask_compress import Compress

    url_prefix = "/v1"
    documentation_endpoint = "/doc/"
    api_v1 = Blueprint('api_v1', __name__, url_prefix=url_prefix)
    home_page = Blueprint('home-page', __name__)

    @home_page.route("/")
    def index_page():
        return 'See <a href="{}{}">Swagger Documentation</a> of this API'.format(url_prefix, documentation_endpoint)

    rdv_api_v1 = Api(api_v1, doc=documentation_endpoint, version="unstable", title='RDV API',
                     description='This is the documentation of the [rdv_query_builder]'
                                 '(https://gitlab.switch.ch/ub-unibas/rdv/services/rdv_query_builder) API. '
                                 'The rdv_query_builder API is the backend to which the angular '
                                 'front-end application [rdv_viewer]'
                                 '(https://gitlab.switch.ch/ub-unibas/rdv/services/rdv_viewer) speaks. '
                                 'Its main duty is to transform queries sent by the front-end '
                                 'to elasticsearch and vice versa.')

    rdv_api_v1.add_namespace(ns_iiifdiscovery)

    rdv_query_app = Flask(__name__)
    rdv_query_app.register_blueprint(api_v1)
    rdv_query_app.register_blueprint(home_page)

    origins = [
        "https?://127.0.0.1.*",
        "https?://localhost.*",
        "https?://ub-.*.unibas.ch",
        "https?://192.168.128.*",
        "https?://.*.ub-digitale-dienste.k8s-001.unibas.ch"
    ]
    CORS(rdv_query_app, origins=origins, supports_credentials=True)
    Compress(rdv_query_app)

    debug = False
    rdv_query_app.run(debug=debug)